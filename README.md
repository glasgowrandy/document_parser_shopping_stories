# Shopping Stories Document Parser Library
A custom parser used to extract data from the Shopping Stories dataset.
This parser acts as the first step in our two step process of importing data into our neo4j instance.
