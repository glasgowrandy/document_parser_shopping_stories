
const neo4j = require("neo4j-driver"); // Imports the neo4j server commands

class Import {
    // constructor
    constructor(_username, _password, _url) {
        this.username = _username;
        this.password = _password;
        this.url = _url;
        this.driver = this._establishDriver();
    }

    // Establish Driver
    _establishDriver() {
        // Validating the user info
        const authToken = neo4j.auth.basic(this.username, this.password);

        // Returns the driver object
        return neo4j.driver(this.url, authToken);
    }

    // Establish Session
    _establishSession() {
        // Returns a connection to the server
        return this.driver.session();
    }

    _queryBuilder(jsonObjToParse, label) {
        // get the keys needed for each object
        const objKeys = Object.keys(jsonObjToParse);
        let query = `CREATE (:${label}{\n`;

        // String Builder
        objKeys.forEach((key, index) => {
            // Doesn't populate the attribute if the value is undefined
            if (jsonObjToParse[key] !== undefined) {
            
                if (objKeys.length - 1 > index) {
                    query += `${key}:"${jsonObjToParse[key]}",\n`;
                } else {
                    query += `${key}:"${jsonObjToParse[key]}"\n`;
                }
            }
        });
        query += `})`;

        // Returning the concatenated query
        return query;
    }

    /**
     *
     * Creates nodes in Neo4j of label Account
     * @param {*} jsonObj
     *
     */
    accounts(jsonObj) {
        const session = this._establishSession();

        const accounts = jsonObj;
        let queryToRun = "";

        accounts.map((cur) => {
            queryToRun += this._queryBuilder(cur, "Account");
        });

        // Running the session and closing the session.
        session
            .run(queryToRun)
            .then(() => session.close())
    }

    // Import Categories
    categories(jsonObj) {
        const session = this._establishSession();
    }

    /**
     *
     * Creates nodes in Neo4j of label People
     * @param {*} jsonObj
     *
     */
    people(jsonObj) {
        const session = this._establishSession();

        const people = jsonObj;
        let queryToRun = "";

        people.map((cur) => {
            queryToRun += this._queryBuilder(cur, "Person");
        });

        // Running the session and closing the session.
        session
            .run(queryToRun)
            .then(() => session.close())
    }

    /**
     *
     * Creates nodes in Neo4j of label Place
     * @param {*} jsonObj
     *
     */
    places(jsonObj) {
        const session = this._establishSession();

        const places = jsonObj;
        let queryToRun = "";

        places.map((cur) => {
            queryToRun += this._queryBuilder(cur, "Place");
        });

        // Running the session and closing the session.
        session
            .run(queryToRun)
            .then(() => session.close())
    }

    /**
     *
     * Creates nodes in Neo4j of label Organization
     * @param {*} jsonObj
     *
     */
    organizations(jsonObj) {
        const session = this._establishSession();

        const organizations = jsonObj;
        let queryToRun = "";

        organizations.map((cur) => {
            queryToRun += this._queryBuilder(cur, "Organization");
        });

        // Running the session and closing the session
        session
            .run(queryToRun)
            .then(() => session.close())
    }

    /**
     *
     * Creates nodes in Neo4j of label Transaction
     * Creates nodes in Neo4j of label Entry
     * @param {*} jsonObj
     *
     */
    transactions(jsonObj) {
        const session = this._establishSession();

        // Objects to be parsed and uploaded
        const transactions = jsonObj["transactions"];
        const entries = jsonObj["entries"];

        // Main Query String
        let queryToRun = "";

        // Building the query for transactions
        transactions.map((cur) => {
            queryToRun += "\n" + this._queryBuilder(cur, "Transaction");
        });

        // Building the query for entries
        entries.map((cur) => {
            queryToRun += "\n" + this._queryBuilder(cur, "Entry");
        });

        // Running the session and closing the session
        session
            .run(queryToRun)
            .then(() => session.close())
    }
}

module.exports = Import;
