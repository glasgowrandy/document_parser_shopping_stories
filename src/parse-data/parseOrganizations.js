function parseOrganization(obj) {
    let organization = {
        name: obj.LastName,
        associated: obj.FirstName,
        prefix: obj.Prefix,
        suffix: obj.Suffix,
        type: obj.Profession,
        category: obj.ProfessionCategory,
        location: obj.Location,
        reference: obj.Reference,
        variations: obj.Variations,
        store: obj.Store,
        account: obj.Account
    }

    if (organization.name) organization.name = organization.name.toLowerCase()
    if (organization.associated) organization.associated = organization.associated.toLowerCase()
    if (organization.prefix) organization.prefix = organization.prefix.toLowerCase()
    if (organization.suffix) organization.suffix = organization.suffix.toLowerCase()
    if (organization.type) organization.type = organization.type.toLowerCase()
    if (organization.category) organization.category = organization.category.toLowerCase()
    if (organization.location) organization.location = organization.location.toLowerCase()
    if (organization.reference) organization.reference = organization.reference.toLowerCase()
    if (organization.variations) organization.variations = organization.variations.toLowerCase()
    if (organization.store) organization.store = organization.store.toLowerCase()
    if (organization.account) organization.account = organization.account.toLowerCase()

    return organization
}

function parseOrganizationDriver(obj) {

    let parsedData = []

    obj.forEach(el => {

        if (el.Reference === 'Organization') {
            parsedData.push(parseOrganization(el))
        }
    })

    return parsedData
}

module.exports = { parseOrganizationDriver }