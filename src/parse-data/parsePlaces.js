function parsePlaces(obj) {
    let place = {
        name: obj.LastName,
        associated: obj.FirstName,
        prefix: obj.Prefix,
        suffix: obj.Suffix,
        type: obj.Profession,
        category: obj.ProfessionCategory,
        location: obj.Location,
        reference: obj.Reference,
        variations: obj.Variations,
        store: obj.Store,
        account: obj.Account
    }

    if (place.name) place.name = place.name.toLowerCase()
    if (place.associated) place.associated = place.associated.toLowerCase()
    if (place.prefix) place.prefix = place.prefix.toLowerCase()
    if (place.suffix) place.suffix = place.suffix.toLowerCase()
    if (place.type) place.type = place.type.toLowerCase()
    if (place.category) place.category = place.category.toLowerCase()
    if (place.location) place.location = place.location.toLowerCase()
    if (place.reference) place.reference = place.reference.toLowerCase()
    if (place.variations) place.variations = place.variations.toLowerCase()
    if (place.store) place.store = place.store.toLowerCase()
    if (place.account) place.account = place.account.toLowerCase()

    return place
}

function parsePlacesDriver(obj) {

    let parsedData = []

    obj.forEach(el => {
        if (el.Reference === 'Place') {
            parsedData.push(parsePlaces(el))
        }
    })

    return parsedData
}

module.exports = { parsePlacesDriver }